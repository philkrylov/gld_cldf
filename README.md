# gld_cldf

Utilities to export [GLD](http://starling.rinet.ru/new100/) data to [CLDF](https://cldf.clld.org/) format.

## Installation

    pip install -U git+https://gitlab.com/philkrylov/gld_cldf

## API

* `gld2cldf(gld_file: str,
            cldf_dir: str,
            language_group: str = None)`

  Read an XLS file in GLD format from `gld_file`, write out CLDF files to `cldf_dir` directory.
  If the file contains etymology number columns, the corresponding language should have a language group defined either in a row with 0 in the first column and `Language group` in the second column, or in the optional `language_group` argument.
  Any warnings are printed to the standard output device, any fatal errors will raise an exception.

## Usage example

    $ python
    Python 3.6.8 (default, Dec 30 2018, 13:01:55) 
    [GCC 4.2.1 Compatible Apple LLVM 9.1.0 (clang-902.0.39.2)] on darwin
    Type "help", "copyright", "credits" or "license" for more information.
    >>> from gld_cldf import gld2cldf
    >>> gld2cldf('/Users/phil/data/ncc/lez.xls', '/Users/phil/data/clld-lez')
    Proto-Lezgian ‛bird’: Unknown character U+0441 at 'сʼʷ...'
    Proto-Lezgian ‛we₂’: Unknown character U+0141 at 'Łä-n...'
    >>> 
