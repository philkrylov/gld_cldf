__version__ = '0.0.3'

import csv
from pathlib import Path
import re

from pycldf import Wordlist

from .transcr import to_ipa
from . import xls_reader

GLD_CONCEPTICON_MAP = {meaning: int(concepticon_id)
                       for _, meaning, concepticon_id
                       in csv.reader(open(Path(__file__).parent / 'data' / 'gld_concepticon.csv'))}

GLD_READERS = {
    '.xls': xls_reader.read,
    '.xlsx': xls_reader.read,
}

RE_REFERENCE = re.compile(r'([A-Z]\D+) ?(\d{4}): (([IVXLCDM0-9-]+, *)*[IVXLCDM0-9-]+)[;.]')

RE_BRACES = re.compile(r"[{⟨][^{⟨]*[}⟩]")


def get_sources(note: str) -> list:
    sources = []
    for match in RE_REFERENCE.finditer(note) if note else []:
        sources.append((match.group(1).replace(' ', '') + match.group(2),
                        ','.join(match.group(3).split(', '))))
    return sources


def remove_sources(note: str) -> str:
    return RE_REFERENCE.sub('', note).strip() if note else None


def form_to_ipa(s: str, context: str) -> str:
    if s == '0.0':
        raise Exception("Looks like a formula. Please re-format the cell as Text")
    s = RE_BRACES.sub('', s)
    return to_ipa(s, context)


def gld2cldf(gld_file: str,
             cldf_dir: str,
             language_group: str = None):
    gld_data = GLD_READERS[Path(gld_file).suffix](gld_file, language_group=language_group)

    all_sources = set()
    parametertable = []
    parameter_id = 0
    for concept, info in gld_data['concepts'].items():
        parameter_id += 1
        parameter = dict(ID=parameter_id,
                         Name=concept,
                         GLD_ID=info['gld_id'],
                         concepticon_ID=GLD_CONCEPTICON_MAP.get(concept))
        info['ID'] = parameter_id
        parametertable.append(parameter)

    formtable = []
    form_id = 0
    for form in gld_data['forms']:
        form_id += 1
        sources = get_sources(form.get('note'))
        all_sources.update((book for book, _ in sources))
        context = "{} ‛{}’".format(form['lang']['name'],
                                   form['concept']['name'])
        try:
            formtable.append(dict(ID=str(form_id),
                                  Form=form['form'] or '—',
                                  Language_ID=form['lang']['ID'],
                                  Parameter_ID=form['concept']['ID'],
                                  Source=['{}[{}]'.format(book, year)
                                          for book, year in sources],
                                  Comment=remove_sources(form.get('note')),
                                  IPA=form_to_ipa(form['form'], context),
                                  Cognate_Class=form.get('etnum')))
        except Exception as e:
            raise Exception("{}: {}".format(context, e))

    languagetable = []
    for lang in gld_data['langs']:
        languagetable.append(dict(ID=lang['ID'],
                                  Name=lang['name'],
                                  Fixation_Date=lang.get('fixation_date'),
                                  Language_Group=lang.get('group')))

    dataset = Wordlist.in_dir(cldf_dir)
    dataset.add_component('ParameterTable')
    dataset.add_component('LanguageTable')
    dataset.add_columns('FormTable', 'IPA', 'Cognate_Class')
    dataset.add_columns('ParameterTable', 'GLD_ID', 'concepticon_ID')
    dataset.add_columns('LanguageTable', 'Fixation_Date', 'Language_Group')
    dataset.write(FormTable=formtable,
                  ParameterTable=parametertable,
                  LanguageTable=languagetable)
    with open(Path(cldf_dir) / 'sources.txt', 'w') as f:
        f.writelines(s + '\n' for s in sorted(all_sources))
