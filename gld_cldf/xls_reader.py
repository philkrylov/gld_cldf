import re

import xlrd


RE_LANG_ID = re.compile(r'[a-zA-Z0-9_\-]+')
TABLESEP = '\u0014'
XLRD_HTML_STYLE_MAP = (
    ('bold', 'b'),
    ('italic', 'i'),
    ('underlined', 'u'),
)


def get_unicode_from_cell(sheet, n_row, n_col) -> str:
    def apply_subscript(s: str, start: int, end: int) -> str:
        for ofs in range(start, (end if end >= 0 else len(s))):
            assert s[ofs].isdigit(), \
                "Can only unicodize subscript digits in {} offset {} row {}, col {}".format(
                    s, ofs, n_row, n_col)
            s = ''.join((s[:ofs], chr(0x2080 + int(s[ofs])), s[ofs + 1:]))
        return s

    value = str(sheet.cell(n_row, n_col).value)
    runlist = sheet.rich_text_runlist_map.get((n_row, n_col))
    if runlist:
        subscript_start = -1
        for offset, font_index in runlist:
            if subscript_start >= 0:
                value = apply_subscript(value, subscript_start, offset)
                subscript_start = -1
            font = sheet.book.font_list[font_index]
            if font.escapement == 2:
                subscript_start = offset
            #elif font.escapement == 1:
            #    raise Exception("Can't process superscript in {} offset {} row {} col {}"
            #                    .format(value, offset, n_row, n_col))
        if subscript_start >= 0:
            value = apply_subscript(value, subscript_start, -1)
    return value


def parse_tables(s: str) -> str:
    # Tables processing
    lines = s.split('\n')
    in_table = False
    out_lines = []
    for line in lines:
        this_out = ''
        if not in_table and line.startswith(TABLESEP):
            this_out += '<table><tbody>'
            in_table = True
        if in_table and not line.startswith(TABLESEP):
            this_out += '</tbody></table>'
            in_table = False
        if in_table:
            this_out += '<tr><td>' + line[1:].replace(TABLESEP, '</td><td>') + '</td></tr>'
        else:
            this_out += line
        if this_out.startswith(('<tr>', '</tbody>')):
            out_lines[-1] += this_out
        else:
            out_lines.append(this_out)
    value = '<br/>'.join(out_lines)
    if in_table:
        value += '</tbody></table>'
    return value


def get_html_from_cell(sheet: xlrd.sheet.Sheet, n_row: int, n_col: int) -> str:
    styles = dict(i=-1)

    def close_styles(s: str, start_pos: int, end_pos: int) -> str:
        start = ''
        end = ''
        for style, state in list(styles.items()):
            if state >= 0:
                start += '<{}>'.format(style)
                end += '</{}>'.format(style)
                styles[style] = -1
        return start + s[start_pos:end_pos] + end

    value = get_unicode_from_cell(sheet, n_row, n_col)
    runlist = sheet.rich_text_runlist_map.get((n_row, n_col))
    if runlist:
        out = ''
        run_start = 0
        for offset, font_index in runlist:
            out += close_styles(value, run_start, offset)
            run_start = offset
            font = sheet.book.font_list[font_index]
            for xlrd_style, html_style in XLRD_HTML_STYLE_MAP:
                styles[html_style] = offset if getattr(font, xlrd_style) else -1
            styles['sup'] = offset if font.escapement == 1 else -1
            styles['sub'] = offset if font.escapement == 2 else -1

        out += close_styles(value, run_start, len(value))
        value = out
    return parse_tables(value)


def read(xls_file: str,
         language_group: str = None):
    workbook = xlrd.open_workbook(filename=xls_file, formatting_info=True)
    for sheet in workbook.sheets():
        notes = []
        forms = []
        concepts = {}
        langs, notes_columns = read_langs(sheet, language_group=language_group)
        for n_row in range(2, sheet.nrows):
            if not sheet.cell(n_row, 0).value:
                print("Skipping row {} with empty NUMBER".format(n_row + 1))
                continue
            concept = read_concept(sheet, n_row, concepts)
            for lang in langs:
                if 'word_column' in lang:
                    word = get_unicode_from_cell(sheet, n_row, lang['word_column']).strip()
                else:
                    word = ''
                if 'number_column' in lang:
                    num = int(sheet.cell(n_row, lang['number_column']).value)
                else:
                    num = 0
                note = (get_html_from_cell(sheet, n_row, lang['notes_column'])
                        if 'notes_column' in lang else '')
                if word or note:
                    form = {'concept': concepts[concept], 'lang': lang, 'form': word, 'num': num}
                    if note:
                        form['note'] = note
                    if 'etnum_column' in lang:
                        form['etnum'] = int(sheet.cell(n_row, lang['etnum_column']).value)

                    forms.append(form)
            for n_column in notes_columns:
                note = get_html_from_cell(sheet, n_row, n_column).strip()
                if note:
                    notes.append({'concept': concept, 'note': note})
        return dict(langs=langs,
                    concepts=concepts,
                    forms=forms,
                    notes=notes)


def read_concept(sheet: xlrd.sheet.Sheet, n_row: int, concepts: dict) -> str:
    concept_name = get_unicode_from_cell(sheet, n_row, 1).strip()
    if concept_name not in concepts:
        concepts[concept_name] = {'name': concept_name, 'gld_id': int(sheet.cell(n_row, 0).value)}
    return concept_name


def read_langs(sheet: xlrd.sheet,
               language_group: str = None) -> tuple:
    if not language_group:
        try:
            family_row = next(n_row for n_row in range(1, sheet.nrows)
                              if (int(sheet.cell(n_row, 0).value) == 0 and
                                  sheet.cell(n_row, 1).value.strip().lower() == 'language group'))
        except StopIteration:
            family_row = None
            print("No Language group info found")

    langs = []
    notes_columns = []
    for n_column in range(sheet.ncols):
        value = sheet.cell(0, n_column).value.strip()
        context = "\"{}\" in column {}".format(value, n_column + 1)
        if n_column == 0:
            assert value.lower() == 'number', "1st column must be named NUMBER"
        elif n_column == 1:
            assert value.lower() == 'word', "2nd column must be named WORD"
        elif value.endswith('#'):
            assert langs[-1]['name'] == value[:-1].strip(), \
                "{}: Number columns should follow word columns".format(context)
            assert 'number_column' not in langs[-1], \
                "{}: Only one number column for a language is allowed".format(context)
            langs[-1]['number_column'] = n_column
        elif value.endswith(' etymology'):
            assert langs[-1]['name'] == value[:-10].strip(), \
                "{}: Etymology number columns should follow word columns".format(context)
            assert 'etnum_column' not in langs[-1], \
                "{}: Only one etymology number column for a language is allowed".format(context)
            assert langs[-1].get('group'), \
                "{}: Etymology number column without Language group info".format(context)
            langs[-1]['etnum_column'] = n_column
        elif 'notes' in value.lower():
            lang_name = value.replace(' notes', '').strip()
            lang_id = lang_name.translate(str.maketrans({' ': '', '(': '_', ')': ''}))
            try:
                lang_entry = next(e for e in langs if e['name'] == lang_name)
            except StopIteration:
                langs.append({'name': lang_name, 'ID': lang_id, 'notes_column': n_column})
                # notes_columns.append(n_column)
            else:
                lang_entry['notes_column'] = n_column
        else:
            lang_id = value.translate(str.maketrans({' ': '', '(': '_', ')': ''}))
            assert RE_LANG_ID.match(lang_id), \
                "{}: Could not generate a valid Language ID".format(context)
            langs.append({'name': value,
                          'word_column': n_column,
                          'ID': lang_id})

            if language_group:
                langs[-1]['group'] = language_group
            elif family_row is not None:
                langs[-1]['group'] = sheet.cell(family_row, n_column).value

            fixation_date = sheet.cell(1, n_column).value
            assert (isinstance(fixation_date, float) or fixation_date.isdigit()), \
                "{}: Invalid or missing date of fixation".format(context)
            langs[-1]['fixation_date'] = int(fixation_date)

    return langs, notes_columns
