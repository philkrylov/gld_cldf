import csv
from pathlib import Path
import re
import unicodedata


GLD_IPA_MAP = [(gld.replace("◌", ""), ipa.replace("◌", ""))
               for gld, ipa in csv.reader(open(Path(__file__).parent / 'data' / 'gld_ipa.csv'))]
SORTED_GLD_TO_IPA = sorted(GLD_IPA_MAP, reverse=True)
RE_KEEP = re.compile(r"^[- ~*=()/,A-Z]")


def to_ipa(s: str, context: str) -> str:
    result = []
    while s:
        if RE_KEEP.match(s):
            gld = s[:1]
            ipa = gld
        else:
            gld, ipa = next(((gld, ipa) for gld, ipa in SORTED_GLD_TO_IPA if s.startswith(gld)),
                            (None, None))
        if gld:
            result.append(ipa)
            s = s[len(gld):]
        else:
            # Try to decompose the first character
            c = unicodedata.normalize('NFD', s[:1])
            if c == s[:1]:
                print("{}: Unknown character U+{:04X} at '{}...'".format(context, ord(s[:1]), s[:20]))
                result.append(s[:1])
                s = s[1:]
            else:
                s = c + s[1:]
    return ''.join(result)
